use tokio::io::{self, AsyncBufReadExt, AsyncWriteExt, BufReader};
use tokio::net::{TcpListener, TcpStream, ToSocketAddrs};

async fn connection_loop(mut stream: TcpStream) -> io::Result<()> {
    let (reader, mut writer) = stream.split();
    let buf_reader = BufReader::new(reader);
    let mut lines = buf_reader.lines();
    while let Some(_line) = lines.next_line().await? {
        writer.write_all("+PONG\r\n".as_bytes()).await?;
    }
    Ok(())
}

async fn accept_loop(addr: impl ToSocketAddrs) -> io::Result<()> {
    let listener = TcpListener::bind(addr).await?;
    loop {
        let (incoming, _) = listener.accept().await?;
        tokio::spawn(async move { connection_loop(incoming).await });
    }
}

#[tokio::main]
async fn main() -> io::Result<()> {
    accept_loop("127.0.0.1:8888").await?;
    Ok(())
}
